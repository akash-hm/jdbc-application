<%@page import="java.util.ArrayList"%>
<%@page import="com.js.dto.Product"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Products</title>

<style type="text/css">

body{
	background-color: grey;
}

table {
	background-color: yellow;
	border: 3px solid black;
	border-radius: 10px;
	box-shadow: 10px 10px 20px black;
}

th{
	border: 3px solid black;
	border-radius: 10px;
	padding: 10px;
}

td{
	border: 3px solid black;
	border-radius: 10px;
	padding: 10px;
	text-align: center;
}

a {
	color: red;
}

td:hover{
	background-color: white;
	color: white;
}

a:hover {
	color: white;
}

</style>

</head>
<body>
	<h1>Here are the Products</h1>
	<table border="5px">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Brand</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Delete Product</th>
		</tr>
		
		<% ArrayList<Product> al = (ArrayList<Product>) request.getAttribute("data"); %>
		<% for(Product p : al) { %>
			<tr>
				<td><%= p.getId() %></td>
				<td><%= p.getName() %></td>
				<td><%= p.getBrand() %></td>
				<td><%= p.getPrice() %></td>
				<td><%= p.getQuantity() %></td>
				<td id="delete-key"><a href="delete?id=<%= p.getId() %>">Delete</a></td>
			</tr>
		<%} %>
		
	</table>
</body>
</html>