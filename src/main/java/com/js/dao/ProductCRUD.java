package com.js.dao;

import com.js.dto.Product;

import java.sql.*;
import java.util.ArrayList;

public class ProductCRUD {
    public static final String URL="jdbc:mysql://localhost:3306/product_store";
    public static final String USER="root";
    public static final String PWD="root@1234";
    public static final String PATH="com.mysql.cj.jdbc.Driver";

    public static int insertProduct(Product p) {
        Connection con = null;
        String query = "insert into product values (?,?,?,?,?)";
        try {
            Class.forName(PATH);
            con = DriverManager.getConnection(URL,USER,PWD);
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, p.getId());
            ps.setString(2, p.getName());
            ps.setString(3, p.getBrand());
            ps.setDouble(4, p.getPrice());
            ps.setInt(5, p.getQuantity());
            return ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int deleteProductByID(int id){
        String query = "delete from product where id=?";
        Connection c = null;
        try {
            Class.forName(PATH);
            c = DriverManager.getConnection(URL,USER,PWD);
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1,id);
            return ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int updateProduct(int id , Product p){
        String query = "update product set name=?,brand=?,price=?,quantity=? where id=?";
        Connection c = null;
        try {
            Class.forName(PATH);
            c = DriverManager.getConnection(URL,USER,PWD);
            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(1,p.getName());
            ps.setString(2,p.getBrand());
            ps.setDouble(3,p.getPrice());
            ps.setInt(4,p.getQuantity());
            ps.setInt(5,id);
            return ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int deleteByProductPrice(double price){
        String query = "delete from product where price=?";
        Connection c = null;
        try {
            Class.forName(PATH);
            c = DriverManager.getConnection(URL,USER,PWD);
            PreparedStatement ps = c.prepareStatement(query);
            ps.setDouble(1,price);
            return ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public Product getproductById(int id){
        String querry = "select * from product where id = ?";
        Connection c = null;
        try {
            Class.forName(PATH);
            c =  DriverManager.getConnection(URL,USER,PWD);
            PreparedStatement ps = c.prepareStatement(querry);
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                Product pro = new Product();
                pro.setId(rs.getInt(1));
                pro.setName(rs.getString(2));
                pro.setBrand(rs.getString(3));
                pro.setPrice(rs.getDouble(4));
                pro.setQuantity(rs.getInt(5));
                return pro;
            }
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public ArrayList<Product> getAllProduct(){
        ArrayList<Product> al = new ArrayList<Product>();
        String querry = "select * from product";
        Connection c = null;
        try {
            Class.forName(PATH);
            c = DriverManager.getConnection(URL,USER,PWD);
            PreparedStatement ps = c.prepareStatement(querry);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Product p = new Product();
                p.setId(rs.getInt(1));
                p.setName(rs.getString(2));
                p.setBrand(rs.getString(3));
                p.setPrice(rs.getDouble(4));
                p.setQuantity(rs.getInt(5));
                al.add(p);
            }
            return al;
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public int[] batchExecuteInsert(ArrayList<Product> products){
        Connection c = null;
        String querry = "insert into product values(?,?,?,?,?)";

        try {
            Class.forName(PATH);
            c = DriverManager.getConnection(URL,USER,PWD);
            PreparedStatement ps = c.prepareStatement(querry);
            for(Product p : products){
                ps.setInt(1,p.getId());
                ps.setString(2,p.getName());
                ps.setString(3,p.getBrand());
                ps.setDouble(4,p.getPrice());
                ps.setInt(5,p.getQuantity());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new int[0];
    }

    public int[] batchExecuteDelete(int[] arr){
        Connection c = null;
        String querry = "delete from product where id = ?";
        try {
            Class.forName(PATH);
            c = DriverManager.getConnection(URL,USER,PWD);
            PreparedStatement ps = c.prepareStatement(querry);
            for(int x : arr) {
                ps.setInt(1, x);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return arr;
    }
}
