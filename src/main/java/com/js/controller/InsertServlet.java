package com.js.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.js.dao.Customer_CRUD;
import com.js.dto.Customer;

@WebServlet("/register")

public class InsertServlet extends HttpServlet {
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Customer c = new Customer();
		int id = Integer.parseInt(req.getParameter("id"));
		c.setId(id);
		c.setName(req.getParameter("name"));
		c.setEmail(req.getParameter("email"));
		int password = Integer.parseInt(req.getParameter("password"));
		c.setPassword(password);
		long phone = Long.parseLong(req.getParameter("phone"));
		c.setPhone(phone);
		int res = Customer_CRUD.insertCustomer(c);
		RequestDispatcher rd = req.getRequestDispatcher("result.jsp");
		if (res > 0) {
			req.setAttribute("msg", "Inserted Successfully");
			rd.forward(req, resp);
		} else {
			req.setAttribute("msg", "Failed to Insert");
			rd.forward(req, resp);
		}
	}
}
