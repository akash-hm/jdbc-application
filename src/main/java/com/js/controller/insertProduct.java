package com.js.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.js.dao.ProductCRUD;
import com.js.dto.Product;

@WebServlet("/save")

public class insertProduct extends HttpServlet{
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Product p = new Product();
		int id = Integer.parseInt(req.getParameter("id"));
		p.setId(Integer.valueOf((req.getParameter("id"))));
		p.setName(req.getParameter("name"));
		p.setBrand(req.getParameter("brand"));
		p.setPrice(Double.valueOf((req.getParameter("price"))));
		p.setQuantity(Integer.valueOf(req.getParameter("quantity")));
		
		RequestDispatcher rd = req.getRequestDispatcher("result.jsp");
		
		int res = ProductCRUD.insertProduct(p);
		if(res > 0) {
			req.setAttribute("msg", "Inserted successfully");
			rd.forward(req, resp);
		} else {
			req.setAttribute("msg", "Not Inserted");
			rd.forward(req, resp);
		}
	}
}
