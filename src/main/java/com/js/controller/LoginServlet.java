package com.js.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.js.dao.Customer_CRUD;

@WebServlet("/login")

public class LoginServlet extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email");
		int password = Integer.valueOf(req.getParameter("password"));
		
		boolean res = Customer_CRUD.checkCustomer(email, password);
		
		if(res) {
			RequestDispatcher rd = req.getRequestDispatcher("home.jsp");
			rd.forward(req, resp);
		} else {
			PrintWriter pw = resp.getWriter();
			pw.write("<html>\n"
					+ "<body>\n"
					+ "	<h1 style=\"color: red;\"> Please check your email or password</h1>\n"
					+ "</body>\n"
					+ "</html>");
			RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
			rd.include(req, resp);
		}
	}
}
